package com.techelevator;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.techelevator.npgeek.JdbcParkDao;
import com.techelevator.npgeek.Park;

public class JdbcHomePageIntegrationTest extends DaoIntegrationTest {
	
	private static SingleConnectionDataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	private JdbcParkDao jdbcParkDao;
	
	
	@BeforeClass
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/npgeek");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");

		dataSource.setAutoCommit(false);
	}

	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}


	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}
	
	public DataSource getDataSource() {
		return dataSource;
	}
	
	@Before
	public void setup() {
		jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcParkDao = new JdbcParkDao(dataSource);
		insertPark("fake park");
	}
	
	@Test
	public void verify_new_park_added() {
		List<Park> parks = jdbcParkDao.getAllParks();
		boolean isReturned = false;
		for(Park park : parks) {
			if(park.getParkName().equals("fake park")) isReturned = true;
		}
		Assert.assertTrue(isReturned);
		}

	private void insertPark(String parkName) {
		String sqlAddNewPark = "INSERT INTO park (parkcode, parkname, state, acreage, elevationinfeet, milesoftrail, "
				+ "numberofcampsites, climate, yearfounded, annualvisitorcount, inspirationalquote, inspirationalquotesource, "
				+ "parkdescription, entryfee, numberofanimalspecies) VALUES ('FPNP', ?, 'Georgia', '123', '12434', '1234', '123', "
				+ "'woodland', '1932', '1234', 'lovely quote', 'someone famous', 'nice park', '10', '234')";
		jdbcTemplate.update(sqlAddNewPark, parkName);
	}
	


}


