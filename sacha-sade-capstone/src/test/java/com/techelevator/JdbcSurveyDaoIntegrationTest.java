package com.techelevator;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;


import com.techelevator.npgeek.JdbcSurveyDao;
import com.techelevator.npgeek.Survey;

public class JdbcSurveyDaoIntegrationTest extends DaoIntegrationTest{
	
	private static SingleConnectionDataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	private JdbcSurveyDao jdbcSurveyDao; 
	String emailAddress;
	
	@BeforeClass
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/npgeek");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");

		dataSource.setAutoCommit(false);
	}

	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}


	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}
	
	public DataSource getDataSource() {
		return dataSource;
	}
	
	@Before
	public void setup() {
		jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcSurveyDao = new JdbcSurveyDao(dataSource);
		insertSurvey("testnpgeek@testnpgeek.com");
	}	
	
	@Test
	public void verify_inserted_survey_appears() {
		List<Survey> surveys = jdbcSurveyDao.getListOfSurveys();
		boolean isReturned = false;
		for(Survey survey : surveys) {
			if(survey.getEmailAddress().equals("testnpgeek@testnpgeek.com")) isReturned = true;
		}
		Assert.assertTrue(isReturned);
	}
	
	
	private void insertSurvey(String emailAddress) {
		String sqlAddNewSurvey = "INSERT INTO survey_result (parkcode, emailaddress, state, activitylevel)\n" + 
			"VALUES ('ENP', ?, 'Georgia', 'active')";
		jdbcTemplate.update(sqlAddNewSurvey, "testnpgeek@testnpgeek.com");
		
	}

}
