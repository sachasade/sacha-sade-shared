<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:import url="/WEB-INF/jsp/header.jsp" />



<div class="home-container">

	<c:forEach var="park" items="${allParks}">
		<div class=home-img-and-info>
			<div class="home-park-image">
				<a href="details?parkCode=${park.parkCode}"><img style=  "min-height: 100%"
					src="img/parks/${park.parkCode}.jpg" alt="imageOf${park.parkName}" /></a>
			</div>
				
			 
			<div class="home-park-info">
				<div class="home-park-name">
					<h4>${park.parkName}</h4>
				</div>
				
				<div class="home-state"><h5>${park.state}</h5></div>
		
				<p>${park.parkDescription}</p>
			
			</div>

		</div>
	</c:forEach>
</div>


<c:import url="/WEB-INF/jsp/footer.jsp" />