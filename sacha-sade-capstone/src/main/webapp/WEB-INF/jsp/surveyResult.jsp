<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:import url="/WEB-INF/jsp/header.jsp">

	<c:param name="pageTitle" value="" />
</c:import>

<h1>Favorite Park Survey Results</h1>

	
<table>
	
 
	<tr align="left" height=40px>
		<th> </th>  <th width=300px>Park Name</th> <th>Total Votes</th>
		</tr>
	<c:forEach var="surveyResult" items="${surveyResults}"> 

	<tr align= "left">	
		<td width= 160px> <img style="width:150px"  src="img/parks/${surveyResult.parkCode}.jpg"></td>
					
		<td width=300px>${surveyResult.parkName}</td>
		<td>${surveyResult.voteCount}</td>
		</tr>
	</c:forEach>	

</table>
<c:import url="/WEB-INF/jsp/footer.jsp" />