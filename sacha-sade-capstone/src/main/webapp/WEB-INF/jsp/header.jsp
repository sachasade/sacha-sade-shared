<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>

<head>
<title>National Park Geek</title>
<c:url var="stylesheetHref" value="/site.css" />
<link rel="stylesheet" href="${stylesheetHref}">
</head>
<body>
	<header>
		<c:url var="logoImgSrc" value="/img/logo.png" />
		<img id="header-image" src="${logoImgSrc}" alt="National Geek Park Logo">
	</header>

	<nav>
		<ul>
			<c:url var="homePageHref" value="/" />
			<li><a href="${homePageHref}">Home</a></li>
			<li><a href="survey">Survey</a></li>
		</ul>
	</nav>