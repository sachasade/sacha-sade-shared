<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:import url="/WEB-INF/jsp/header.jsp" />

<Section id="main-content">
	<div class="details-container">
		<c:url value="/details?parkCode=${park.parkCode}" var="detailsURL" />
		<h2>${park.parkName}</h2>

		<img id="details-park-image" style="padding-right: 20px"
			src="img/parks/${park.parkCode}.jpg" alt="imageOf${park.parkName}" />

		<div class="details-park-info">
			<h4 id="details-park-name">${park.parkName},${park.state}</h4>
			<h5 id="details-quote">"${park.inspirationalQuote}." -
				${park.inspirationalQuoteSource}</h5>
			<p>Founded in ${park.yearFounded}, ${park.parkDescription}</p>
			<p>Stats:</p>
			<p>Acres: ${park.acreage}</p>
			<p>Elevation: ${park.elevationInFeet} feet</p>
			<p>Miles of Trail: ${park.milesOfTrail} miles</p>
			<p>Number of camp sites: ${park.numberOfCampsites}</p>
			<p>Climate: ${park.climate}</p>
			<p>Annual Visitors: ${park.annualVisitorCount}</p>
			<p>Entry Fee: $${park.entryFee}</p>
			<p>Number of Animal Species: ${park.numberOfAnimalSpecies}</p>
		</div>
		
</div>

</Section>





<c:import url="/WEB-INF/jsp/footer.jsp" />