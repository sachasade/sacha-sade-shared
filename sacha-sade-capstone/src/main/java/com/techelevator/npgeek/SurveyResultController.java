package com.techelevator.npgeek;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes({"request", "parkCode"})
public class SurveyResultController {
	
	@Autowired
	private SurveyResultDao surveyResultDao;
	
	@RequestMapping(path="surveyResult", method=RequestMethod.GET)
	public String displaySurveyResultPage(HttpServletRequest request) {
		List<SurveyResult> surveyResults = surveyResultDao.getSurveyResults();
		request.setAttribute("surveyResults", surveyResults);
		return "/surveyResult";
	}	

}
