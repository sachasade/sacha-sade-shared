package com.techelevator.npgeek;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JdbcSurveyResultDao implements SurveyResultDao {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JdbcSurveyResultDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<SurveyResult> getSurveyResults(){
	List<SurveyResult> surveyResults = new ArrayList<>();
	String sqlGetSurveyResults = "SELECT survey_result.parkcode, park.parkname, COUNT(survey_result.parkcode) AS votecount\n" + 
			"FROM survey_result\n" + 
			"JOIN park ON park.parkcode = survey_result.parkcode\n" + 
			"GROUP BY survey_result.parkcode, park.parkname\n" + 
			"ORDER BY votecount DESC, parkname ASC";
	SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetSurveyResults);
	while(results.next()) {
		surveyResults.add(mapRowToSurveyResults(results));
	}
	return surveyResults;
}

	private SurveyResult mapRowToSurveyResults(SqlRowSet row) {
		SurveyResult surveyResult = new SurveyResult();
		surveyResult.setParkCode(row.getString("parkcode").toLowerCase());
		surveyResult.setParkName(row.getString("parkname"));
		surveyResult.setVoteCount(row.getString("votecount"));
		
		return surveyResult;
	}



	


	}




	

