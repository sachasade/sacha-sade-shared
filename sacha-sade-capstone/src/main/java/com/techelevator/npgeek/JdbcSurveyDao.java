package com.techelevator.npgeek;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JdbcSurveyDao implements SurveyDao {

	private JdbcTemplate jdbcTemplate;
	
	Long surveyId;
	
	public JdbcSurveyDao(DataSource datasource) {
		this.jdbcTemplate = new JdbcTemplate(datasource);
	}
	
	@Override
	public void addSurveyToDatabase(Survey survey) {
		String sqlInsertSurvey = "INSERT INTO survey_result (parkcode, emailaddress, state, activitylevel) " + 
				"VALUES (?, ?, ?, ?)";
		jdbcTemplate.update(sqlInsertSurvey, survey.getParkCode(), survey.getEmailAddress(), survey.getState(), survey.getActivityLevel());
		
	}

//	private Long getSurveyId(String emailAddress) {
//		String sqlGetSurveyId = "SELECT surveyid FROM survey_result\n" + 
//				"WHERE emailaddress = ?";
//		SqlRowSet surveyIdResult = jdbcTemplate.queryForRowSet(sqlGetSurveyId, emailAddress);
//		surveyId = 0l;
//		if(surveyIdResult.next()) {
//			surveyId = surveyIdResult.getLong("surveyId");
//		}
//		return surveyId;
//	}

	@Override
	public List<Survey> getListOfSurveys() {
		List<Survey> surveys = new ArrayList<Survey>();
		String sqlSelectAllSurveys = "SELECT *, LOWER(parkcode) as LowerParkCode FROM survey_result";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlSelectAllSurveys);
		while(result.next()) {
			surveys.add(mapRowToSurvey(result));
		}
		return surveys;
	}

	private Survey mapRowToSurvey(SqlRowSet row) {
		Survey survey = new Survey();
		survey.setSurveyId(row.getLong("surveyId"));
		survey.setParkCode(row.getString("parkcode").toLowerCase());
		survey.setEmailAddress(row.getString("emailaddress"));
		survey.setState(row.getString("state"));
		survey.setActivityLevel(row.getString("activitylevel"));
		return survey;
	}

}
