package com.techelevator.npgeek;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
	public class JdbcParkDao implements ParkDao {
	
	private JdbcTemplate jdbcTemplate;
	
	
	@Autowired
	public JdbcParkDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Park> getAllParks() {
		List<Park> allParks = new ArrayList<>();
		String sqlSelectAllParks = "SELECT *, LOWER(parkcode) as LowerParkCode FROM park";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllParks);
		while(results.next()) {
			allParks.add(mapRowToPark(results));
		}
		return allParks;
	}

	
	private Park mapRowToPark(SqlRowSet row) {
		Park park = new Park();
		park.setParkCode(row.getString("parkcode").toLowerCase());
		park.setParkName(row.getString("parkname"));
		park.setState(row.getString("state"));
		park.setParkDescription(row.getString("parkdescription"));
		park.setAcreage(row.getInt("acreage"));
		park.setElevationInFeet(row.getInt("elevationinfeet"));
		park.setNumberOfCampsites(row.getInt("numberofcampsites"));
		park.setMilesOfTrail(row.getDouble("milesOfTrail"));
		park.setClimate(row.getString("climate"));
		park.setYearFounded(row.getInt("yearFounded"));
		park.setAnnualVisitorCount(row.getInt("annualVisitorCount"));
		park.setInspirationalQuote(row.getString("inspirationalQuote"));
		park.setInspirationalQuoteSource(row.getString("inspirationalQuoteSource"));
		park.setEntryFee(row.getInt("entryFee"));
		park.setNumberOfAnimalSpecies(row.getInt("numberOfAnimalSpecies"));
		return park;
	}
	
	@Override
	public Park getParkByParkCode(String parkCode) {
		Park newPark = null;
		String sqlSelectAllParksByParkCode = "SELECT * FROM park WHERE parkCode = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllParksByParkCode, parkCode);
	
		if(results.next()) {
			newPark = mapRowToPark(results);	
		}
		return newPark;
	}

	@Override
	public void save(Park newPark) {	
	}
	
		
	}


