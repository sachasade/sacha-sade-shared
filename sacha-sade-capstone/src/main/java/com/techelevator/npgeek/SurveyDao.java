package com.techelevator.npgeek;

import java.util.List;

public interface SurveyDao {
	
	public List<Survey> getListOfSurveys();
	
	public void addSurveyToDatabase(Survey survey);
		
		
}
