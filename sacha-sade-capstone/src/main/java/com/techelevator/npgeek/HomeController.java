package com.techelevator.npgeek;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@SessionAttributes({"request", "parkCode"})

public class HomeController {

	@Autowired
	private ParkDao parkDao;
	
//	@Autowired
//	private SurveyDao surveyDao;

//	@Autowired
//	private SurveyResultDao surveyResultDao;
	
	
	@RequestMapping("/")
	public String displayHomePage(HttpServletRequest request) {
		List<Park> allParks = parkDao.getAllParks();
		request.setAttribute("allParks", allParks);
		return "homePage";
	}

	@RequestMapping("/details")
	public String displayDetailsPage(HttpServletRequest request) {
		List<Park> allParks = parkDao.getAllParks();
		request.setAttribute("allParks", allParks);
		return "details";
	}
	@RequestMapping(path="/details", method=RequestMethod.GET)
	public String showParkDetails(HttpServletRequest request, ModelMap map) {
		String parkCode = request.getParameter("parkCode");
		map.addAttribute("park", parkDao.getParkByParkCode(parkCode.toUpperCase()));
		return "details";
	}
//	@RequestMapping(path="/survey", method=RequestMethod.GET)
//	public String displaySurveyPage(Model model) {
//		if(! model.containsAttribute("survey")) {
//			model.addAttribute("survey", new Survey());
//		}
//		
//	return "/survey";
//	}
//	
//	
//	@RequestMapping(path="survey", method=RequestMethod.POST)
//	public String processSurveyInput(@Valid @ModelAttribute("survey") Survey survey,
//									BindingResult result,
//									RedirectAttributes attr,
//									ModelMap model) {
//		if(result.hasErrors()) {
//			return "/survey";
//		}
//		
//		surveyDao.addSurveyToDatabase(survey);
//		model.addAttribute("participantSurvey", survey);	
//		
//		return "redirect:/surveyInput";
//	}
//	
//	@RequestMapping(path="surveyInput", method=RequestMethod.GET)
//	public String displaySurveyInputPage(HttpServletRequest request) {
//		List<SurveyResult> surveyResults = surveyResultDao.getSurveyResults();
//		request.setAttribute("surveyResults", surveyResults);
//		return "/surveyInput";
//	}
	


}
