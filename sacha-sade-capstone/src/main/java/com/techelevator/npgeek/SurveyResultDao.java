package com.techelevator.npgeek;

import java.util.List;

public interface SurveyResultDao {
	
public List<SurveyResult> getSurveyResults();
}
