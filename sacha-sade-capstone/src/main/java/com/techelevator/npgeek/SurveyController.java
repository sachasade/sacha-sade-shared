package com.techelevator.npgeek;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@SessionAttributes({"request", "parkCode"})
public class SurveyController {
	
	@Autowired
	private SurveyDao surveyDao;
	
	
	@RequestMapping(path="/survey", method=RequestMethod.GET)
	public String displaySurveyPage(Model model) {
		if(! model.containsAttribute("survey")) {
			model.addAttribute("survey", new Survey());
		}
		
	return "/survey";
	}
	
	
	@RequestMapping(path="survey", method=RequestMethod.POST)
	public String processSurveyInput(@Valid @ModelAttribute("survey") Survey survey,
									BindingResult result,
									RedirectAttributes attr,
									ModelMap model) {
		if(result.hasErrors()) {
			return "/survey";
		}
		
		surveyDao.addSurveyToDatabase(survey);
		model.addAttribute("participantSurvey", survey);	
		
		return "redirect:/surveyResult";
	}
	
//	@RequestMapping(path="surveyInput", method=RequestMethod.GET)
//	public String displaySurveyInputPage(HttpServletRequest request) {
//		List<SurveyResult> surveyResults = surveyResultDao.getSurveyResults();
//		request.setAttribute("surveyResults", surveyResults);
//		return "/surveyInput";
//	}

}
